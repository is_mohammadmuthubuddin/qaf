Feature: Google Search
	 @Smoke
  Scenario: Login Page
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
 
  
     @Smoke
  Scenario: Add Product to Cart
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User select an Item and add to the cart
    
     @Smoke
  Scenario: Remove Product from Cart 
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User Remove selected Item From the cart
    
     
     @Smoke
  Scenario: User validate the cart
   Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
   Then User validate selected Item added to the cart 
    
    
     @Smoke
  Scenario: User filter the products by prise
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User filter the products by prise 
    
      @Smoke
  Scenario: User validate social Twitter account text
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User Verify Twitter Logo text with "${Account}" and "${Twitter}"
   
    
       @Smoke
  Scenario: User continue the shopping from the cart page
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User click continue shopping from Cart page
    
    
       @Smoke
  Scenario: User checkout from the shopping from the cart page
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User click checkout button shopping from Cart page
    
    
     @Smoke
  Scenario: User checkout from the shopping cart and Enter the user information in Checkout page
    Given User already in Login page
    When User give userID and Password and click submit button
    And HomePage is displayed
    Then User click checkout button shopping from Cart page
    And User Enter FirstName,LastName Zip code and Click continue 

	 @Smoke
  Scenario: User checkout from the shopping cart and Enter the user information in Checkout page
    Meta-data:{"dataFile":"resources/data/logintestdata.xls", "sheetName":"LoginSheet"}
    Given User already in Login page
     When User login with "${Username}" and "${Password}"
    And HomePage is displayed
    Then User click checkout button shopping from Cart page
    And User Enter FirstName,LastName Zip code and Click continue 
	
   