package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.getText;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.submit;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;


import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.example.pages.CheckOutYourInformationPage;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.ProductPage;
import com.qmetry.qaf.example.pages.YourCartPage;

public class OnlineShoppingProductsSteps {
	

	/**
	 * @param searchTerm
	 *            : search item to be searched
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User select an Item and add to the cart")
	public static String selectItem() throws InterruptedException {
		HomePage home = new HomePage();
		
		ProductPage product = new ProductPage();
		home.waitForPageToLoad();
		String itemName =home.selectBackPackBag();
		return itemName;
		
	}
	
	/**
	 * @param 
	 *            : remove the selected item
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User Remove selected Item From the cart")
	public static void removeItemFromItem()  {
		HomePage home = new HomePage();
		
		ProductPage product = new ProductPage();
		home.waitForPageToLoad();
		product.removeProductFromCart();
		
		
	}
	
	/**
	 * @param 
	 *            : user item  to cart
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User validate selected Item added to the cart")
	public static void validateItemAddedToCart() throws InterruptedException  {


		HomePage home = new HomePage();
		ProductPage product = new ProductPage();
		YourCartPage cart = new YourCartPage();
		String itemselected = home.selectBackPackBag();
		Thread.sleep(5000);
		product.clickShoppingCartButton();
		String itemName= cart.getProductName();
		Assert.assertEquals(itemselected, itemName);
		
	}
	
	/**
	 * @param 
	 *            : user sort the product by prise
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User filter the products by prise")
	public static void productSortByPrise() throws InterruptedException  {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		ProductPage product = new ProductPage();
		product.selectProductByPrise(); 
		Thread.sleep(7000);
		
	}
	
	/**
	 * @param 
	 *            : user click on continue shopping from the chart page
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User click continue shopping from Cart page")
	public static void continueShopping() throws InterruptedException  {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		ProductPage product = new ProductPage();
		product.clickShoppingCartButton();
		YourCartPage cart = new YourCartPage();
		cart .clickContinueShopping();
		
	}
	
	/**
	 * @param 
	 *            : user sort the product by prise
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User Verify Twitter Logo text with {0} and {1}")
	public static void verifySocialAccountLogo() throws InterruptedException  {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		ProductPage product = new ProductPage();
		product.verifySocialAccount("Twitter") ;
		Thread.sleep(7000);
		
	}
	
	/**
	 * @param 
	 *            : user click on continue shopping from the chart page
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User click checkout button shopping from Cart page")
	public static void checkOutShopping() throws InterruptedException  {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		ProductPage product = new ProductPage();
		product.clickShoppingCartButton();
		YourCartPage cart = new YourCartPage();
		cart .clickCheckOut();
		
	}
	//
	
	/**
	 * @param 
	 *            : user click on continue shopping from the chart page
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User Enter FirstName,LastName Zip code and Click continue")
	public static void enterUserDetails() throws InterruptedException  {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		ProductPage product = new ProductPage();
		product.clickShoppingCartButton();
		YourCartPage cart = new YourCartPage();
		cart .clickCheckOut();
		CheckOutYourInformationPage checkout = new CheckOutYourInformationPage();
		checkout.enterUserDetails();
		
	}
}
