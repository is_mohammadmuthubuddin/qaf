package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.getText;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.submit;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;


import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LoginPage;

public class loginStep {
	


	/**
	 * @param searchTerm
	 *            : search term to be searched
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User already in Login page")
	public static void userloginPage() throws InterruptedException {
		LoginPage loginPage = new LoginPage();
		loginPage.openPage(null, null);
		Thread.sleep(6000);
	}
	
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 * @throws InterruptedException 
	 */
	
	@QAFTestStep(description = "User login with {0} and {1}")
	public static void login(String userName,String Password) throws InterruptedException {
		LoginPage loginPage = new LoginPage();
		loginPage.login(userName, Password);
		Thread.sleep(600);
	}
	
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "User give userID and Password and click submit button")
	public static void userlogin() throws InterruptedException {
		HomePage home = new HomePage();
		home.launchPage(null);
		home.waitForPageToLoad();
		Thread.sleep(600);
	}
	
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "HomePage is displayed")
	public static void homePageisDisplayed() throws InterruptedException {
		HomePage home = new HomePage();
		home.waitForPageToLoad();
		Thread.sleep(600);
	}

	
}
