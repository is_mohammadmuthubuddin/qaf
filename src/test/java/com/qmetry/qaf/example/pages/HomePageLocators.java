package com.qmetry.qaf.example.pages;

public interface HomePageLocators {
	static final String HEADER_LOC = "{'locator':'css=.title';'desc':'Header of Page'}";
	static final String BackPackBag_Loc = "{\"locator\":\"xpath=//div[text()='Sauce Labs Backpack']\",\"desc\":\"Produc details\"}";
	static final String SauceBikeLight_Loc = "{\"locator\":\"xpath=//div[text()='Sauce Labs Bike Light']\",\"desc\":\"Produc details\"}";
	
	static final String addToCart_LOC = "{'locator':'css=#add-to-cart-sauce-labs-backpack';'desc':'Add to card button'}";
	static final String ShoppingCartHeader_LOC = "{'locator':'css=.primary_header';'desc':Shopping cart Link button'}";
	static final String ShoppingCart_LOC = "{\"locator\":\"xpath=//a[@class='shopping_cart_link']\",\"desc\":\"Produc details\"}";
	static final String BikeLight_Loc = "{\"locator\":\"xpath=//button[@id='add-to-cart-sauce-labs-bike-light']\",\"desc\":\"Produc details\"}";
}
