package com.qmetry.qaf.example.pages;

import java.util.Iterator;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<LoginPage> implements HomePageLocators {

	@FindBy(locator = HEADER_LOC)
	private QAFWebElement header;

	@FindBy(locator = BackPackBag_Loc)
	private QAFWebElement backPackBag;

	@FindBy(locator = SauceBikeLight_Loc)
	private QAFWebElement sauceBikeLight;

	@FindBy(locator = addToCart_LOC)
	private QAFWebElement addToCart;

	@FindBy(locator = ShoppingCart_LOC)
	private QAFWebElement shoppingCartButton;

	@FindBy(locator = ShoppingCartHeader_LOC)
	private QAFWebElement shoppingCartHeader;

	@FindBy(locator = BikeLight_Loc)
	private QAFWebElement bikeLightAddToCart;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		parent.launchPage(null);
		parent.waitForPageToLoad();
		parent.login("standard_user", "secret_sauce");

	}

	@Override
	public void waitForPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	public String selectBackPackBag() throws InterruptedException {
		String itemName = this.backPackBag.getText();
		this.addToCart.click();
		super.waitForPageToLoad();
		Thread.sleep(30);
		return itemName;
	}

	public String selectBikeLight() throws InterruptedException {
		String itemName = this.sauceBikeLight.getText();
		this.bikeLightAddToCart.click();
		super.waitForPageToLoad();
		Thread.sleep(30);
		return itemName;
	}

	public void waitForShoppingPageToLoad() {
		super.waitForPageToLoad();
		this.shoppingCartHeader.assertPresent();
	}

	public void validateNoofItemsInCart() throws InterruptedException {
		int count = 0;

		this.selectBackPackBag();
		Thread.sleep(3000);
		this.selectBikeLight();
		Thread.sleep(3000);

		ProductPage product = new ProductPage();
		String items = product.noOfProductsInCart();
		System.out.println(" no of items added in cart is" + items);
	}
}