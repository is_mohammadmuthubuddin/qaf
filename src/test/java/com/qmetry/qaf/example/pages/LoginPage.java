package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.qmetry.qaf.automation.step.BaseTestStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage> implements LoginPageLocators {
	//WebDriver driver;
	
@FindBy(locator = User_Name)
private QAFWebElement username;


@FindBy (locator = User_Password)
private QAFWebElement password;

@FindBy (locator = Button_Submit)
private QAFWebElement submit;

	@Override
	public void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	@Override
	public void waitForPageToLoad() {
	    super.waitForPageToLoad();
	   this.username.assertPresent();
	}
	public void login(String userName,String password) {
		this.username.clear();
		this.username.sendKeys(userName);
		this.password.clear();
		this.password.sendKeys(password);
		this.submit.click();
	}
	
	
	
}
