package com.qmetry.qaf.example.pages;

public interface LoginPageLocators {
	static final String User_Name = "{'locator':'name=user-name';'desc':'UserName text box in Page'}";
	static final String User_Password = "{'locator':'name=password';'desc':'UserName text box in Page'}";
	static final String Button_Submit = "{'locator':'name=login-button';'desc':'UserName text box in Page'}";
	
								
}
