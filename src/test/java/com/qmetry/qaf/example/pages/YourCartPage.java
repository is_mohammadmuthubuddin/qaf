package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class YourCartPage extends WebDriverBaseTestPage<ProductPage> implements YourCartPageLocators {

	@FindBy(locator = HEADER_LOC)
	private QAFWebElement header;

	@FindBy(locator = productName_Loc)
	private QAFWebElement productName;

	@FindBy(locator = ContinueShoppingBtn_LOC)
	private QAFWebElement continueShopping;
	
	@FindBy(locator = CheckoutBtn_LOC)
	private QAFWebElement checkout;

	@Override
	public void waitForPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	public void waitForShoppingPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public String getProductName() throws InterruptedException {
		Thread.sleep(5000);
		super.waitForPageToLoad();
		String itemName = this.productName.getText();
		super.waitForPageToLoad();
		return itemName;


	}

	public void clickContinueShopping() {
		super.waitForPageToLoad();
		this.continueShopping.click();
	}
	
	public void clickCheckOut() {
		super.waitForPageToLoad();
		this.checkout.click();
	}
}