package com.qmetry.qaf.example.pages;

public interface ProductPageLocators {
	static final String HEADER_LOC = "{'locator':'css=.title';'desc':'Header of Page'}";
	static final String BackPackBag_Loc = "{\"locator\":\"xpath=//div[text()='Sauce Labs Backpack']\",\"desc\":\"Produc details\"}";
	static final String addToCart_LOC = "{'locator':'css=#add-to-cart-sauce-labs-backpack';'desc':'Add to card button'}";
	static final String ShoppingCartHeader_LOC = "{'locator':'css=.primary_header';'desc':Shopping cart Link button'}";
	//static final String ShoppingCart_LOC = "{\"locator\":\"xpath=//a[@class='shopping_cart_link']\",\"desc\":\"Produc details\"}";
	static final String RemoveBackPackBag_Loc = "{\"locator\":\"xpath=//button[@name='remove-sauce-labs-backpack']\",\"desc\":\"Produc details\"}";
	static final String productName_Loc = "{\"locator\":\"xpath=//div[@class='inventory_item_name']\",\"desc\":\"Produc details\"}";
	static final String ShoppingCart_LOC = "{\"locator\":\"xpath=//a[@class='shopping_cart_link']\",\"desc\":\"Produc details\"}";
	static final String ProductSortDropDown_Loc = "{\"locator\":\"xpath=//select[@class='product_sort_container']\",\"desc\":\"Produc details\"}";
	static final String Twitter_Loc = "{\"locator\":\"xpath=//a[text()='Twitter']\",\"desc\":\"Twitter details\"}";
}
