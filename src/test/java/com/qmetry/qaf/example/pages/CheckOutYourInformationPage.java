package com.qmetry.qaf.example.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CheckOutYourInformationPage extends WebDriverBaseTestPage<YourCartPage> implements CheckoutYourInformationPageLocators {

	@FindBy(locator = HEADER_LOC)
	private QAFWebElement header;

	@FindBy(locator =FirstNameInputBox_LOC)
	private QAFWebElement firstName;

	@FindBy(locator =LastNameInputBox_LOC)
	private QAFWebElement lastName;
	
	@FindBy(locator =ZipCodeInputBox_LOC)
	private QAFWebElement zipcode;
	
	@FindBy(locator =Continue_LOC)
	private QAFWebElement continueBtn;
	
	@Override
	public void waitForPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	public void waitForShoppingPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public void enterUserDetails()  {
		super.waitForPageToLoad();
		this.firstName.sendKeys("test");
		this.lastName.sendKeys("test2");
		this.zipcode.sendKeys("560048");
		this.continueBtn.click();
		
	}
	
	

	
}