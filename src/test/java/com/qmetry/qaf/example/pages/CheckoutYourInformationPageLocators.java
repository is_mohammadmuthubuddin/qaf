package com.qmetry.qaf.example.pages;

public interface CheckoutYourInformationPageLocators {
	static final String HEADER_LOC = "{'locator':'css=.title';'desc':'Header of Page'}";
	static final String CheckoutBtn_LOC = "{\"locator\":\"xpath=//button[@id='checkout']\",\"desc\":\"Produc details\"}";
	static final String FirstNameInputBox_LOC = "{\"locator\":\"xpath=//input[@id='first-name']\",\"desc\":\"Produc details\"}";
	static final String LastNameInputBox_LOC = "{\"locator\":\"xpath=//input[@id='last-name']\",\"desc\":\"Produc details\"}";
	static final String ZipCodeInputBox_LOC = "{\"locator\":\"xpath=//input[@id='postal-code']\",\"desc\":\"Produc details\"}";
	static final String Continue_LOC = "{\"locator\":\"xpath=//input[@id='continue']\",\"desc\":\"Produc details\"}";
	
}
