package com.qmetry.qaf.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductPage extends WebDriverBaseTestPage<HomePage> implements ProductPageLocators {

	@FindBy(locator = HEADER_LOC)
	private QAFWebElement header;

	@FindBy(locator = BackPackBag_Loc)
	private QAFWebElement backPackBag;

	@FindBy(locator = addToCart_LOC)
	private QAFWebElement addToCart;

	@FindBy(locator = ShoppingCart_LOC)
	private QAFWebElement shoppingCartButton;

	@FindBy(locator = ShoppingCartHeader_LOC)
	private QAFWebElement shoppingCartHeader;

	@FindBy(locator = RemoveBackPackBag_Loc)
	private QAFWebElement removeButton;
	
	@FindBy(locator = productName_Loc)
	private QAFWebElement productName;
	
	@FindBy(locator = ProductSortDropDown_Loc)
	private QAFWebElement  productSortDropDown;
	
	
	@FindBy(locator = Twitter_Loc)
	private QAFWebElement  twitteraccount;
	
	
	
	@Override
	public void waitForPageToLoad() {
		super.waitForPageToLoad();
		this.header.assertPresent();
	}

	public String selectBackPackBag()  {
		String itemName = this.backPackBag.getText();
		this.addToCart.click();
		super.waitForPageToLoad();
		
		return itemName;
	}

	public String clickShoppingCartButton() throws InterruptedException  {
		Thread.sleep(5000);
		super.waitForPageToLoad();
		this.shoppingCartButton.click();
		super.waitForPageToLoad();
		return null;

	}
	public String noOfProductsInCart() throws InterruptedException  {
		Thread.sleep(5000);
		super.waitForPageToLoad();
		String items =this.shoppingCartButton.getText();
		super.waitForPageToLoad();
		return items;
	

	}
	
	
	public void waitForShoppingPageToLoad() {
		super.waitForPageToLoad();
		this.shoppingCartHeader.assertPresent();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}
	
	public void removeProductFromCart()  {
		super.waitForPageToLoad();
		this.removeButton.click();
		super.waitForPageToLoad();
		

	}
	
	public void selectProductByPrise() throws InterruptedException {
		Thread.sleep(8000);
		Select filter = new Select(productSortDropDown);
		filter.selectByVisibleText("Price (low to high)");  
		
	}
	
	public String verifySocialAccount(String account) throws InterruptedException {
		Thread.sleep(8000);
		String actualText =this.twitteraccount.getText();
		Assert.assertEquals(account, actualText);
		System.out.println("Account logo is"+actualText);
		return actualText;
	}
	
}