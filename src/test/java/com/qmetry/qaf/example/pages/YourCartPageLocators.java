package com.qmetry.qaf.example.pages;

public interface YourCartPageLocators {
	static final String HEADER_LOC = "{'locator':'css=.title';'desc':'Header of Page'}";
	static final String productName_Loc = "{\"locator\":\"xpath=//div[@class='inventory_item_name']\",\"desc\":\"Produc details\"}";
	static final String ContinueShoppingBtn_LOC = "{'locator':'css=#continue-shopping';'desc':'Continue Shopping Button'}";
	static final String CheckoutBtn_LOC = "{\"locator\":\"xpath=//button[@id='checkout']\",\"desc\":\"Produc details\"}";
	
}
