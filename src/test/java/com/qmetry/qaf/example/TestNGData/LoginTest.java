package com.qmetry.qaf.example.TestNGData;



import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;
import com.qmetry.qaf.automation.util.ExcelUtil;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LoginPage;

public class LoginTest {
	//QAFWebDriver driver;

	@BeforeMethod
	public void setUp() {
		
		LoginPage loginPage = new LoginPage();
		loginPage.openPage(null, null);
		
	}
	
	@DataProvider()
	public Object getLoginData() {
		Object[][] testData =ExcelUtil.getExcelData("resources\\TestData .xlsx", false, "Sheet1");
		return testData;
	}
	
	
	@Test
	public void Login(String Username,String Password) {
		LoginPage loginPage = new LoginPage();
		loginPage.openPage(null, null);
		loginPage.waitForPageToLoad();
		loginPage.login(Username, Password);
	}
}
