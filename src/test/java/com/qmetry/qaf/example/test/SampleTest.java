package com.qmetry.qaf.example.test;

import org.testng.annotations.Test;
import static com.qmetry.qaf.automation.step.CommonStep.get;
import static com.qmetry.qaf.automation.step.CommonStep.verifyLinkWithPartialTextPresent;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.example.steps.StepsLibrary.searchFor;
import static com.qmetry.qaf.example.steps.loginStep.*;

import java.util.Map;

import org.openqa.selenium.interactions.ClickAction;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.ExcelUtil;
import com.qmetry.qaf.example.pages.HomePage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.ProductPage;
import com.qmetry.qaf.example.pages.YourCartPage;
import com.qmetry.qaf.example.steps.loginStep;

public class SampleTest extends WebDriverTestCase {

//	@Test
//	public void testGoogleSearch() {
//		get("/");
//		searchFor("qaf github infostretch");
//		verifyLinkWithPartialTextPresent("qaf");
//		
//	}

//	@Test
//	public void login() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//
//		HomePage home = new HomePage();
//		ProductPage product = new ProductPage();
//		home.launchPage(null);
//		home.waitForPageToLoad();
//
//	}
//
//	@Test
//	public void addToCart() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//
//		HomePage home = new HomePage();
//		ProductPage product = new ProductPage();
//		home.launchPage(null);
//		home.waitForPageToLoad();
//
//		String itemselected = home.selectBackPackBag();
//		
//	}
	
//	@Test
//	public void removeProductFromCart() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//
//		HomePage home = new HomePage();
//		ProductPage product = new ProductPage();
//		home.launchPage(null);
//		home.waitForPageToLoad();
//		//home.waitForShoppingPageToLoad();
//		String itemselected = home.selectBackPackBag();
//		Thread.sleep(4000);
//		product.removeProductFromCart();
//		Thread.sleep(4000);
////	home.clickShoppingCartButton();
//	}
	
	
//	@Test
//	public void validateViewCart() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//
//		HomePage home = new HomePage();
//		ProductPage product = new ProductPage();
//		YourCartPage cart = new YourCartPage();
//		home.launchPage(null);
//		home.waitForPageToLoad();
//		//home.waitForShoppingPageToLoad();
//		String itemselected = home.selectBackPackBag();
//		Thread.sleep(5000);
//		product.clickShoppingCartButton();
//		String itemName= cart.getProductName();
//		Assert.assertEquals(itemselected, itemName);
//	}
	
//	@Test
//	public void continueShoppingFromCartPage() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//
//		HomePage home = new HomePage();
//		ProductPage product = new ProductPage();
//		YourCartPage cart = new YourCartPage();
//		home.launchPage(null);
//		home.waitForPageToLoad();
//		Thread.sleep(4000);
//		product.clickShoppingCartButton();
//		cart .clickContinueShopping();
//		
//	}
//	@QAFDataProvider(dataFile = "resources/data/logintestdata.xls", sheetName="LoginSheet")
//	@Test
//	public void productFilter() throws InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		loginPage.openPage(null, null);
//		//loginPage.login(null, null);
//		
//
////		HomePage home = new HomePage();
////		ProductPage product = new ProductPage();
//
//	}
	
	
//	@QAFDataProvider(dataFile = "resources/data/logintestdata.xls", sheetName="LoginSheet")
//	@Test(description = "login functionality test")
//	public void login(Map<String, Object> data) throws InterruptedException {
//		System.out.println("user Name > " + data.get("Username") + " Password > " +data.get("Password"));
//				
//	    LoginPage loginPage = new LoginPage();
//	   loginPage.openPage(null, null);
//	    loginPage.launchPage(null);
//	    Thread.sleep(3000);
//	    // loginPage.logins(data.get("Username"), data.get("Password"));
//	     loginPage.logins("standard_user", "secret_sauce");
//	     
//	   //Validator.verifyThat(status, Matchers.equalTo(data.get("isvalid"));
//	   // loginPage.getErrorMessage().verifyText(data.get("expected_msg"));
//	}
	
	@QAFDataProvider(dataFile = "resources/data/logintestdata.xls", sheetName="LoginSheet")
	@Test(description = "login functionality test")
	public void logins(Map<String, Object> data) throws InterruptedException {
		
				
	    LoginPage loginPage = new LoginPage();
	   loginPage.openPage(null, null);
	    loginPage.launchPage(null);
	    Thread.sleep(3000);
	    String un =(String) data.get("Username");
	    String pwd =(String) data.get("Password");
	    
	    loginPage.login(un,pwd);
	   // loginPage.login("standard_user", "secret_sauce");
	     
	}
}
